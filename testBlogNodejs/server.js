var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var morgan = require('morgan');
var mongoose = require('mongoose');
var User = require('./app/models/user');
var bodyParser = require('body-parser');
var router = express.Router();
var appRoutes = require('./app/routers/api')(router);
var path = require('path');


app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(morgan('dev'));
app.use('/api', appRoutes);
app.use(express.static(__dirname + '/public'));

mongoose.connect('mongodb://hoangdung:hoangdung14254506@ds135394.mlab.com:35394/testblognodejs', function (err) {
    if (err) {
        console.log('Not connect to the database ' + err);
    } else {
        console.log('Successfully connected to MongoDB');
    }
});

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/views/index.html'));
});

app.listen(port, function () {
    console.log('Running the server on port ' + port);
});