angular.module('mainControllers', ['authServices'])

    .controller('mainCtrl', function ($http, $location, $timeout, Auth) {

        var app = this;
        
        this.doLogin = function (loginData) {

            app.loading = true;
            app.errorMsg = false;
        
            Auth.login(app.loginData).then(function (data) {
                if (data.data.success) {
                    app.loading = false;
                    app.successMsg = data.data.message + '....Redirecting';
                    $timeout(function () {
                        $location.path('/');
                    }, 2000);
                } else {
                    app.loading = false;
                    app.errorMsg = data.data.message;
                }
            });
        };
    });